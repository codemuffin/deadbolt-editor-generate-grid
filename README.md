# Deadbolt Editor - Generate Items Grid

Quick script used for modding Deadbolt.

Converts JSON exported from [this spreadsheet](https://docs.google.com/spreadsheets/d/1XsQPEOL5d5vsu0OXlZaMR01yTMRrRebbrrtjea6FaHU) to [GML](https://docs.yoyogames.com/index.html).

Used when adding new items to the modded editor.
