
// Dependancies
// ============================================================================

const FS = require('fs');


// Config
// ============================================================================

const sJsonFileName = 'input.json';
const sOutputFilename = 'output.gml';


// Data Stores
// ============================================================================

// Main string output
let sMainOutput = '';

// Store for comment headings, to build TOC later (sTOC)
let aCommentHeadings = [];

// Empty grid
let sEmptyGridSection = '';

// Store for tab_grid assigment, done last
const aTabGridAssignments = [
	'\n',
	'tab_grid[0] = empty_grid // Used when no sidebar is open'
];

const oGrids = {
	wall_grid: {
		nicename: "Wall Tool",
		items: [],
	},
	struct_grid: {
		nicename: "House",
		items: [],
	},
	music_grid: {
		nicename: "Music",
		items: [],
	},
	enemy_grid: {
		nicename: "Enemies",
		items: [],
	},
	cover_grid: {
		nicename: "Cover",
		items: [],
	},
	weapon_grid: {
		nicename: "Weapons",
		items: [],
	},
	obj_grid: {
		nicename: "Objectives",
		items: [],
	},
	light_grid: {
		nicename: "Lighting",
		items: [],
	},
	pipe_grid: {
		nicename: "Piping System",
		items: [],
	},
	misc_grid: {
		nicename: "Miscellaneous",
		items: [],
	},
	trigger_grid: {
		nicename: "Trigger System",
		items: [],
	},
};


// Read JSON file
// ============================================================================

const sFileContents = FS.readFileSync( './' + sJsonFileName, 'utf8' );
const oRows = JSON.parse( sFileContents );


// Main
// ============================================================================

// Add to grid object
for ( const iRow in oRows )
{
	if ( oRows.hasOwnProperty( iRow ) )
	{
		const oRow = oRows[iRow];

		oGrids[oRow.GRID].items.push( oRow );
	}
}

let iGridIndex = 0;

// Loop through grid objects & their arrays
for ( const sGridKey in oGrids )
{
	if ( oGrids.hasOwnProperty( sGridKey ) )
	{
		const oGrid = oGrids[sGridKey];
		const sGridName = oGrid.nicename;
		const aGridItems = oGrid.items;
		const iItemsCount = aGridItems.length;

		let iGridNum = iGridIndex + 1;
		let sGridNumLeadingZero = ( iGridNum < 10 ) ? `0${iGridNum}` : `${iGridNum}`;
		let sCommentHeading = `// [${sGridNumLeadingZero}] ${sGridName} (${iItemsCount})`;

		// Add to headings array, to build TOc later
		aCommentHeadings.push( sCommentHeading );

		// Comment headings
		sMainOutput += sCommentHeading;
		sMainOutput += `\n// ============================================================================`;
		sMainOutput += `\n`;

		// Create grid
		sMainOutput += `\n${sGridKey} = ds_grid_create(${iItemsCount}, tgrid_y)`;
		sMainOutput += `\ntindex = 0`;
		sMainOutput += `\n`;

		// @DEBUG
		// console.log(aGridItems);

		// Loop through assets
		aGridItems.forEach( oAsset => // eslint-disable-line no-loop-func
		{
			// Handle missing NI_DESC safely
			oAsset.Desc = ( !oAsset.Desc ) ? "" : oAsset.Desc;

			let sEscapedName = '"' + oAsset.Name_in_editor.replace( /"/g, '" + chr(34) + "' ) + '"';
			let sEscapedDesc = '"' + oAsset.Desc.replace( /"/g, '" + chr(34) + "' ) + '"';

			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Sprite_index,   ${oAsset.Sprite_index})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Name_in_editor, ${sEscapedName})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Object_index,   ${oAsset.Object_index})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Sprite_name,    ${oAsset.Sprite_name})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Object_name,    ${oAsset.Object_name})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Ignore,         ${oAsset.Ignore})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Desc,           ${sEscapedDesc})`;
			sMainOutput += `\nds_grid_set(${oAsset.GRID}, tindex, oNI.TAB_Since,          ${oAsset.Since})`;
			sMainOutput += `\ntindex++`;
		});

		// Add space below
		sMainOutput += `\n\n\n`;

		// Add to tab_grid assigments
		aTabGridAssignments.push( `tab_grid[${iGridNum}] = ${sGridKey}` );
	}

	iGridIndex++;
}


// Initialise named indexes
// ============================================================================

const sNamedIndexes = `/// Set up tab grids
// ============================================================================

var tindex, tgrid_y, tweapon_grid_size_vanilla, tweapon_grid_new_items, tweapon_grid_new_size, tmisc_vanilla_count, tmisc_modextras_count;

// Named indexes - Set in oNI
// oNI.TAB_Sprite_index   = 0 // vanilla
// oNI.TAB_Name_in_editor = 1 // vanilla
// oNI.TAB_Object_index   = 2 // vanilla
// oNI.TAB_Sprite_name    = 3
// oNI.TAB_Object_name    = 4
// oNI.TAB_Ignore         = 5
// oNI.TAB_Desc           = 6
// oNI.TAB_Since          = 7

// tgrid_x = n // x = grid width,  the number of items
tgrid_y = 8    // y = grid height, the number of properties`;



// Empty grid
// ============================================================================

let sEmptyGridHeading = '// [00] Empty Grid';

aCommentHeadings.unshift( sEmptyGridHeading );

sEmptyGridSection = sEmptyGridHeading +`
// ============================================================================

// Used when no tab is selected
empty_grid = ds_grid_create(0, 0)`;



// Assign to tab grid + commented debug script
// ============================================================================

let sTabGridHeading = '// [X01] Assign to tab_grid';

aCommentHeadings.push( sTabGridHeading );

sTabGridHeading += `\n// ============================================================================`;

aTabGridAssignments.unshift( sTabGridHeading );


// Commented debug script
// ============================================================================

// Add commented debug lines
aTabGridAssignments.push( '\n\n' );
aTabGridAssignments.push( `///[DISABLED] DebugLog: oMenu grids` ); // debug
aTabGridAssignments.push( `//log_ds_grids_tabs()` ); // debug


// Concat final strings
// ============================================================================

// Join arrays for final strings
const sTOC = aCommentHeadings.join( '\n' );
const sTabGridAssignments = aTabGridAssignments.join( '\n' );

const aOutputStrings = [
	sNamedIndexes,
	sTOC,
	sEmptyGridSection,
	sMainOutput,
	sTabGridAssignments,
];

// Final output, to save
// const sOutput = sTOC + '\n\n\n' + sMainOutput + '\n\n\n' + sTabGridAssignments;
const sOutput = aOutputStrings.join( '\n\n\n' );

// @DEBUG
// console.log(sMainOutput);
// console.log(sTOC);


// Write output file
// ============================================================================

FS.writeFileSync( sOutputFilename, sOutput, 'utf8' );

console.log('DONE');
